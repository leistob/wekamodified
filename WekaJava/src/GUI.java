import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

/**
 * Created by Matthias on 23.03.2015.
 */
public class GUI extends JFrame{
    JFrame frame;

    private JTabbedPane tabbedPane;

    private ButtonListener buttonListener = new ButtonListener();

    private JButton startButton = new JButton("Start"), openFileButton = new JButton("Datei öffnen");

    private JLabel arffPathLabel = new JLabel("Geben Sie den Dateipfad der arff-Datei ein:"), modeLabel = new JLabel("Wählen Sie einen Modus aus:"),
            firstClassifierLabel = new JLabel("Wählen Sie den ersten Classifier aus:"), secondClassifierLabel = new JLabel("Wählen Sie den zweiten Classifier aus:"),
            thirdClassifierLabel = new JLabel("Wählen Sie den dritten Classifier aus:"), filterLabel = new JLabel("Wählen Sie einen Filter aus:");

    private JTextField arffPathField = new JTextField();

    public JComboBox modeBox, firstClassifierBox, secondClassifierBox, thirdClassifierBox, filterBox;

    private String [] firstClassifierSelection = {"Test1", "Test2"}, secondClassifierSelection = {"Test3", "Test4"},
            thirdClassifierSelection = {"Test5", "Test6"}, filterSelection = {"Test7", "Test8"};

    private java.util.List<JLabel> centerLabel = Arrays.asList(firstClassifierLabel, secondClassifierLabel, thirdClassifierLabel, filterLabel);
    private java.util.List<JComboBox> centerBox = Arrays.asList(firstClassifierBox, secondClassifierBox, thirdClassifierBox, filterBox);
    private java.util.List<String> centerActioncommand = Arrays.asList("firstClassifier", "secondClassifier", "thirdClassifier", "filter");
    private java.util.List<String[]> centerSelection = Arrays.asList(firstClassifierSelection, secondClassifierSelection, thirdClassifierSelection, filterSelection);

    public void createFrame() {
        frame = new JFrame("Modified Weka");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(300, 300, 600, 400);

        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Modus", createFirstPanel());
        tabbedPane.addTab("Classifizierer", createSecondPanel());
        tabbedPane.addTab("Testoptionen", createThridPanel());
        frame.add(tabbedPane);

        frame.setResizable(false);
        frame.setVisible(true);

    }

    private JPanel createFirstPanel() {
        JPanel firstPanel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        firstPanel.setLayout(gbl);

        Font font = arffPathLabel.getFont();
        Font newFont = new Font(font.getName(), font.getStyle(), 15);

        addComponent(firstPanel, gbl, new JLabel(), 0, 0, 1, 11);
        addComponent(firstPanel, gbl, new JLabel(), 1, 0, 6, 1);

        arffPathLabel.setFont(newFont);
        addComponent(firstPanel, gbl, arffPathLabel, 1, 1, 4, 1);
        openFileButton.setBackground(new Color(192, 192, 192));
        addComponent(firstPanel, gbl, openFileButton, 5, 1, 2, 1);
        addComponent(firstPanel, gbl, new JLabel(), 1, 2, 6, 1);
        addComponent(firstPanel, gbl, arffPathField, 1, 3, 6, 1);

        for (int i = 0; i <3; i++) {
            addComponent(firstPanel, gbl, new JLabel(), 1, 4 + i, 6, 1);
        }

        modeLabel.setFont(newFont);
        addComponent(firstPanel, gbl, modeLabel, 1, 7, 4, 1);
        addComponent(firstPanel, gbl, modeBox = createComboBox("mode", new String[] {"automatisch", "manuell"}, 0), 5, 7, 2, 1);

        addComponent(firstPanel, gbl, new JLabel(), 1, 8, 6, 1);

        startButton.setBackground(new Color(192, 192, 192));
        addComponent(firstPanel, gbl, startButton, 1, 9, 6, 1);

        addComponent(firstPanel, gbl, new JLabel(), 1, 10, 6, 3);
        addComponent(firstPanel, gbl, new JLabel(), 8, 0, 1, 12);
        return firstPanel;
    }

    private JPanel createSecondPanel() {
        JPanel secondPanel = new JPanel();


        GridBagLayout gbl = new GridBagLayout();
        secondPanel.setLayout(gbl);

        Font font = firstClassifierLabel.getFont();
        Font newFont = new Font(font.getName(), font.getStyle(), 15);

        int rowLabel = 0;
        for (JLabel label : centerLabel) {
            label.setFont(newFont);
            addComponent(secondPanel, gbl, label, 1, rowLabel, 4, 1);

            rowLabel = rowLabel + 3;
        }
        int rowBox = 1;
        int rowComponents = 0;
        for (JComboBox box : centerBox) {
            addComponent(secondPanel, gbl, box = createComboBox(centerActioncommand.get(rowComponents), centerSelection.get(rowComponents), 0),
                    1, rowBox, 4, 1);
            addComponent(secondPanel, gbl, new JLabel(), 1, rowBox+1, 4, 1);

            box.setEnabled(false);

            rowBox = rowBox + 3;
            rowComponents = rowComponents + 1;
        }
        return secondPanel;
    }

    private JPanel createThridPanel() {
        JPanel thirdPanel = new JPanel();
        return thirdPanel;
    }

    private JComboBox createComboBox(String actionCommand, String[] items, int select) {
        JComboBox box = new JComboBox(items);
        box.setBackground(Color.WHITE);
        box.setFont(new Font(Font.DIALOG, 0, 12));
        box.setSelectedIndex(select);
        box.addActionListener(buttonListener);
        box.setActionCommand(actionCommand);
        return box;
    }

    private static void addComponent(Container cont, GridBagLayout gbl, Component c, int x, int y, int width,
                                     int height) {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = x;
        gbc.gridy = y;
        gbc.gridwidth = width;
        gbc.gridheight = height;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbl.setConstraints(c, gbc);
        cont.add(c);
    }
}
