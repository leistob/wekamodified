/**
 * Created by Matthias on 15.03.2015.
 */
import weka.classifiers.Evaluation;
import weka.classifiers.rules.ZeroR;
import weka.classifiers.meta.CVParameterSelection;
import weka.classifiers.meta.Grading;
import weka.classifiers.functions.SMO;
import weka.core.Instances;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.lang.Double;import java.lang.Exception;import java.lang.String;import java.lang.System;import java.util.Random;

public class UseClassifier {

    public Instances openData(BufferedReader in) throws Exception {
        System.out.println("Geben Sie den Dateipfad der arff-Datei ein:");
        String input = in.readLine();
        BufferedReader reader = new BufferedReader(
                //Pfad und Dateiname der arff-Datei
                new FileReader(input));
        Instances data = new Instances(reader);
        reader.close();
        data.setClassIndex(data.numAttributes() - 1);
        return data;
    }

    public void callZeroRClassifier(Instances data) throws Exception {
        Evaluation eval = new Evaluation(data);
        Random rand = new Random(1);  // using seed = 1
        int folds = 10;
        ZeroR zeroR = new ZeroR();
        zeroR.buildClassifier(data);
        System.out.println("Classifier: ZeroR");
        eval.crossValidateModel(zeroR, data, folds, rand);
        System.out.println(eval.toSummaryString());
        System.out.println(eval.toClassDetailsString());
        System.out.println(eval.toMatrixString());
    }

    public void callCVParameterSelectionClassifier(Instances data) throws Exception {
        Evaluation eval = new Evaluation(data);
        Random rand = new Random(1);  // using seed = 1
        int folds = 10;
        CVParameterSelection cvParameterSelection = new CVParameterSelection();
        cvParameterSelection.buildClassifier(data);
        System.out.println("Classifier: CVParameterSelection");
        eval.crossValidateModel(cvParameterSelection, data, folds, rand);
        System.out.println(eval.toSummaryString());
        System.out.println(eval.toClassDetailsString());
        System.out.println(eval.toMatrixString());
    }

    public void callGradingClassifier(Instances data) throws Exception {
        Evaluation eval = new Evaluation(data);
        Random rand = new Random(1);  // using seed = 1
        int folds = 10;
        Grading grading = new Grading();
        grading.buildClassifier(data);
        System.out.println("Classifier: Grading");
        eval.crossValidateModel(grading, data, folds, rand);
        System.out.println(eval.toSummaryString());
        System.out.println(eval.toClassDetailsString());
        System.out.println(eval.toMatrixString());
    }

    public void callSMOClassifier(Instances data) throws Exception {
        Evaluation eval = new Evaluation(data);
        Random rand = new Random(1);  // using seed = 1
        int folds = 10;

        String[] options = new String[2];
        options[0] = "-C";
        double variableC = 1.1;
        SMO smo = new SMO();

        for (int i = 0; i < 10; i++) {
            options[1] = Double.toString(variableC);
            smo.setOptions(options);
            smo.buildClassifier(data);
            System.out.println("Classifier: SMO");
            System.out.printf("C = %1.1f\n", variableC);
            System.out.println(smo);
            eval.crossValidateModel(smo, data, folds, rand);
            System.out.println(eval.toSummaryString());
            System.out.println(eval.toMatrixString());
            variableC = variableC + 0.1;
        }
    }
}
