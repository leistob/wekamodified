package arffclasses;

import java.io.*;


/**
 * Creats one whole .arff out of the valid information of every given .arff file
 */
public class ArffMatcher {

    private static final String inputPath = "C:\\Users\\Admin\\Desktop\\test";
    private static final String outputPath = inputPath + "\\finalFile";

    private static final String classNames = "@attribute class {Apple,Banana,Biscuit,Crisp,Haribo,Nectarine,No_Food}";

    //Names of all files in directory
    private static String[] allFiles;
    //Names of all files in directory, ending splitted
    private static String[][] allFilesSplit;
    //Total number of files in directory
    private static int numberOfFiles;

    private static int numberAttributes;


    public static void main (String[] args) {

        System.out.println("Problems will occurr, if there are other files than .arrfs in the directory.");

        readInPath(inputPath);
        createArff(inputPath);
    }

    /**
     * Reads files of given path
     */
    private static void readInPath(String path) {

        //Defines number of files, names
        File f = new File(path);
        allFiles = f.list();
        numberOfFiles = allFiles.length;
        allFilesSplit = new String [numberOfFiles][2];

        int numberArffs = 0;
        for(int i=0; i<numberOfFiles; i++) {
            allFilesSplit[i] = allFiles[i].split("\\.");
            if(allFilesSplit[i][1].equals("arff"))
                numberArffs++;
        }

    }


    /**
     * Creates one .arff file compound out of the .arffs in path directory
     */
    private static void createArff(String path) {

        StringBuilder endString = new StringBuilder();
        String line;
        String[] temp;

        //Creating FileReader for first file
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(path + "/" + allFiles[0]));
        } catch (FileNotFoundException e) {
            System.err.println("Problems with creating FileReader respectively file");
            e.printStackTrace();
        }

        //Reading in first file line by line
        try {
            while ((line = in.readLine()) != null) {
                //line @attribute class numeric needs to be replaced
                if(!line.equals("@attribute class numeric"))
                    if(line.equals("@data")) {
                        endString.append(line + "\n");
                        while((line = in.readLine()) != null) {
                            if(line.equals(""))
                                continue;
                            temp = line.split("\\,");
                            numberAttributes = temp.length;
                            //System.out.println(temp.length);
                            temp[0] = "'" + allFiles[0] + "'";
                            for (int j = 0; j < temp.length - 1; j++) {
                                endString.append(temp[j] + ",");
                            }

                        }
                        endString.append("?\n");

                    } else
                        endString.append(line + "\n");
                else
                    endString.append(classNames +"\n");
            }
        } catch (IOException e) {
            System.err.println("Problems with reading in lines of file");
            e.printStackTrace();
        }



        //Reading in all the other .arffs, but only data part
        for(int i=1; i<numberOfFiles; i++) {

            //Creating FileReader for next .arff
            try {
                in = new BufferedReader(new FileReader(path + "/" + allFiles[i]));
            } catch(FileNotFoundException e) {
                System.err.println("Problems with creating FileReader respectively file");
                e.printStackTrace();
            }

            //Reading in current .arff line by line (only data part)
            try{

                while ((line = in.readLine()) != null) {

                    //Runs through file until @data is reached
                    if (line.equals("@data")) {

                        //Appends all lines as of @data (no blank lines)
                        while ((line = in.readLine()) != null) {
                            if (line.equals(""))
                                continue;
                            temp = line.split("\\,");
                            if(!(numberAttributes==temp.length)) {
                                throw new IllegalArgumentException();
                            }
                            temp[0] = "'" + allFiles[i] + "'";
                            for (int j = 0; j < temp.length - 1; j++) {
                                endString.append(temp[j] + ",");
                            }
                        }
                        endString.append("?\n");
                    }

                }
                System.out.println(allFiles[i] + " successfully appended to big .arff");

            } catch (IllegalArgumentException iae) {
                System.err.println(allFiles[i] + " failed to append, because of different number of attributes.");
            }
            catch (IOException e) {
                System.err.println("Problems with reading in lines of file");
            }

        }

        writeInFile(endString);

    }

    /**
     * Takes String and writes it in new generated .arff file
     */
    private static void writeInFile (StringBuilder finalString) {

        //Constructs new path for created files
        File dir = new File(outputPath);
        dir.mkdir();

        PrintWriter pWriter = null;
        try {
            pWriter = new PrintWriter(new BufferedWriter(new FileWriter(outputPath + "/final_file.arff")));
            pWriter.println(finalString);
        } catch (IOException ioe) {
            System.err.println("Problems with creating new file");
            ioe.printStackTrace();
        } finally {
            if (pWriter != null){
                pWriter.flush();
                pWriter.close();
            }
        }
    }
}