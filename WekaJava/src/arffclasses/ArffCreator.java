package arffclasses;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Admin on 19.03.2015.
 */
public class ArffCreator {

    private static final String inputPath = "C:\\Users\\Admin\\Desktop\\ubuntu\\Testarffs";
    private static final String configPath = "/home/tobi/Downloads/openSMILE-2.1.0/config/IS13_ComParE.conf";
    private static final String outputPath = inputPath + "\\arffFiles";

    //Names of all files in directory
    private static String[] allFiles;
    //Names of all files in directory, ending splitted
    private static String[][] allFilesSplit;
    //Total number of files in directory
    private static int numberOfFiles;
    //Total number of .wav file in directory
    private static int numberOfWavs;


    public static void main(String[] args) {

        testPath(inputPath);
        createArffs(inputPath, configPath, outputPath);

    }

    /**
     * Reads in valid information of directory with .wavs in it
     */
    private static void testPath(String path) {

        File f = new File(path);
        //Read out number and name of files from "path"-directory
        allFiles = f.list();
        numberOfFiles = allFiles.length;
        allFilesSplit = new String [numberOfFiles][2];

        //Counts .wav files and splits allFiles in name and file extension
        for(int i=0; i<numberOfFiles; i++) {
            allFilesSplit[i] = allFiles[i].split("\\.");
            if(allFilesSplit[i][1].equals("wav"))
                numberOfWavs++;
        }

        System.out.println(".wav files in directory:  " + numberOfWavs);
        System.out.println("Other files in directory: " + (numberOfFiles-numberOfWavs));
        //System.out.println("Problems will appear, if there are other files in the directory.");
        //System.out.println("Storage on desktop.");



    }


    /**
     * Creates .arffs out of the .wav files in the inputFolder command prompt and SMILExtract
     */
    private static void createArffs(String inputPath, String configPath, String outputPath) {


        ///////////////////////////////////////////////
        //WINDOWS code, works with "ipconfig"

        String command;
        ProcessBuilder builder;
        Process proc = null;


        //Constructs new path for created files
        File dir = new File(outputPath);
        dir.mkdir();

        for (int i = 0; i<1; i++) {

            command = "SMILExtract" + " -C " + configPath +
                    " -I " + inputPath       + "\\" + allFiles[i] +
                    " -O " + outputPath + "\\" + allFilesSplit[i][0] +".arff";

            try {
                builder = new ProcessBuilder("ipconfig");
                proc = builder.start();
                Scanner s = new Scanner( proc.getInputStream() ).useDelimiter( "\\Z" );
                System.out.println( s.next() );
                System.out.println(allFiles[i] + " successfully converted in .arff");

            } catch (IOException e) {
                System.err.println(allFiles[i] + " failed to convert in .arff, because of error in command line prompt.");
                e.printStackTrace();
            }


        }

        ////////////////////////////////////////////////


        /**
         *
         * LINUX code (working under ubuntu 14.04)
         *
        String command;
        Process proc = null;

        for (int i = 0; i<1; i++) {

            command = "SMILExtract" + " -C " + configPath +
                    " -I " + inputPath       + "/" + allFiles[i] +
                    " -O " + outputPath + "/" + allFilesSplit[i][0] +".arff";

            try {
                proc = Runtime.getRuntime().exec("ipconfig");
                System.out.println(allFiles[i] + " successfully converted in .arff");
            } catch (IOException e) {
                System.err.println(allFiles[i] + " failed to convert in .arff, because of error in command line prompt.");
            }


        }
        **/


    }

}

