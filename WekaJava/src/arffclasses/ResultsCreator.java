package arffclasses;

import java.io.*;
import java.text.DecimalFormat;


/**
 * Creates final file (testxxxx-names matched with predicted label)
 */
public class ResultsCreator {

    private static final String desktopString = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\Grid";
    private static final String infoPath = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\Prob_30pred.txt";

    public static void main(String[] args) {

        createResults(infoPath);

    }

    private static void createResults(String path) {

        BufferedReader in = null;
        StringBuilder endString = new StringBuilder();
        String line;
        int counter = 0;
        String[] temp;
        String info = "";
        DecimalFormat df = new DecimalFormat("0000");

        //Reading in the first .arff with attributs and data
        try {
            in = new BufferedReader(new FileReader(path));
            while ((line = in.readLine()) != null) {
                if(counter>=133886 && counter<134355) {
                    temp = line.split(":");
                    int counter2 = 0;
                    while(temp[1].charAt(counter2)!=' ') {
                        info += temp[1].charAt(counter2);
                        counter2++;
                    }
                    System.out.println("test" + df.format((counter-133886+1)) + ";" + info);
                    endString.append("test" + df.format((counter-133886+1)) + ";" + info + "\n");
                    info = "";

                }
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Creates the final file with all .arffs matched together
        PrintWriter pWriter = null;
        try {
            pWriter = new PrintWriter(new BufferedWriter(new FileWriter(desktopString + "/test_labeled.csv")));
            pWriter.println(endString);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (pWriter != null){
                pWriter.flush();
                pWriter.close();
            }
        }

    }



}
