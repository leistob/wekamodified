package arffclasses;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/**
 * Appends the actual labels to the big .arff (recommended for training)
 */
public class ArffInfoAppender {

    private static final String desktopString = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\";
    private static final String inputPath = "C:\\Users\\Admin\\Desktop\\1ComParE2015_Eating.ComParE.Prob30.te.arff";
    private static final String tsvPath = "C:\\Users\\" + System.getProperty("user.name") + "\\Desktop\\ComParE2015_Eating.tsv";

    //Watch out for suitable position of the training information!!!!
    private static final int informationPosition = 4;

    //Number of information in given file
    private static int numberOfInformation;

    //Symbol for splitting the information of the file (wether it is \t or \,)
    private static String splitChar;
    
    //List for a eventuelly read-in of tsv-file
    private static List<String> stringTSV = new ArrayList<String>();

    public static void main(String[] args) {
        stringTSV = readTsvIn(tsvPath);
        numberOfInformation = stringTSV.size();

        System.out.println(System.getProperty("os.name"));
        //createArff(inputPath);
    }


    /**
     * Appends data from .tsv behind every sample
     */
    private static void createArff(String path) {

        BufferedReader in = null;
        StringBuffer endString = new StringBuffer();
        String line;
        int counter = 0;
        String[] temp;

        //Reading in the first .arff with attributs and data
        try {
            in = new BufferedReader(new FileReader(path));
            while ((line = in.readLine()) != null) {
                if(!line.equals("@attribute class numeric"))
                    //Line 6380 contains the data from the .arff
                    if(counter>=6378 && counter <7325) {
                        temp = line.split("\\,");
                        for (int j = 0; j < temp.length - 1; j++) {
                            endString.append(temp[j] + ",");
                        }
                        //In case of trainset, further information is added instead of a '?' at the end
                        //endString.append(stringTSV.get(counter+1-6380) + "\n");
                        endString.append("?\n");
                    } else
                        endString.append(line + "\n");
                else
                    endString.append("@attribute class {Apple,Banana,Biscuit,Crisp,Haribo,Nectarine,No_Food}\n");
                counter++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Creates the final file with all .arffs matched together
        PrintWriter pWriter = null;
        try {
            pWriter = new PrintWriter(new BufferedWriter(new FileWriter(desktopString + "/final_file.arff")));
            pWriter.println(endString);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (pWriter != null){
                pWriter.flush();
                pWriter.close();
            }
        }

    }


    /**
     * Writes data from entered file in a list
     */
    private static List<String> readTsvIn(String path) {

        List<String> tsvInformation = new ArrayList<String>();
        String[] stringMemory;

        //Finds out, wether file is .csv or .tsv
        try{
            String[] tempString = path.split("\\\\");
            tempString = tempString[tempString.length-1].split("\\.");
            splitChar = tempString[1].equals("tsv") ? "\\t" : "\\," ;

        } catch (ArrayIndexOutOfBoundsException aio) {
            System.err.println("You didn't choose any .csv or .tsv file");
            aio.printStackTrace();
        }

        BufferedReader in;
        String line;
        //Reads in the .tsv line by line and stores information in stringMemory
        try {
            in = new BufferedReader(new FileReader(tsvPath));

            while ((line = in.readLine()) != null) {
                stringMemory = line.split(splitChar);
                tsvInformation.add(stringMemory[informationPosition]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tsvInformation;
    }




}
